# Revision history for PragmaCode Gitlab Page V2

The version numbers below try to follow the conventions at http://semver.org/.

## Unreleased

## v1.2.0 - 11/05/2022

* Add Rubens Gomes information
* Add Isaque Alves information
* Add Arthur Vieira information
* SEO fixes
* Upgrade to NodeJS 16
* Upgrade dependencies
* Add sonar to pipeline

## v1.1.0 - 14/04/2021

* Adjust technical advice form text
* Adjust text and layout of technical advice components
* Adjust text and layout of technical advice first section
* Add technical advice landing page images
* Add contact button to technical advice landing page bottom
* Create property to configure LandingPageSection bgcolor
* Create property to configure LandingPageSection column order
* Create LandingPageSection component
* Use smaller navbar logo image in small screens
* Allow custom text in contact form submit button
* Add contact form to technical advice landing page
* Extract Contact Form to component
* Create landing page navbar
* Create technical advice landing page
* Upgrade dependencies

## v1.0.0 - 05/01/2021

* Update Andre's photo
* Create robots.txt
* Disable preload plugin
* Inline Google fonts CSS
* Disable prefetch plugin
* Extract font-awesome to component
* Move Bootstrap code from main.js to views
* Upgrade dependencies
* Configure bootstrap and its dependencies to sepparate webpack chunk
* Preload Google fonts
* Preload cover image
* Add cover image sized specifically for small screens
* Set images size attributes
* Remove unused CSS
* Resize and reduce quality of large images
* Import only the Bootstrap JS parts that are used
* Replace jquery by jquery.slim
* Lazy load components
* Fix sections missing bottom padding
* Adjust TeamMember photo size to medium sized screens
* Create font size adjustment for medium screens
* Adjust small screens font size
* Adjust cover content to medium sized screens
* Fix navbar hidding services and cases title on iphone 5
* Fix services and cases close icon not showing on iOS
* Prerender the application (install prerender-spa-plugin)
* Direct user to status page after contact submission
* Create view with success/error message after contact
* Perform contact form submission using axios
* Install axios
* Fix contact form integration with netlify

## v0.1.0 - 30/11/2020

* Set footer content
* Create Contact form
* Create Team content
* Create CaseDescription pages
* Create Cases list content
* Create Customers list content
* Add Google Analytics tracking code
* Create metatags
* Create PageNotFound page
* Create ServiceDescription pages
* Install font-awesome
* Install vue-router
* Close responsive menu after click
* Create Services description
* Set smooth scroll
* Set the favicon
* Create Principles content
* Create About content
* Create Cover content
* Add locale selection to Navbar
* Create Navbar content
* Set frontend font-family
* Create components for each of the frontend's sections
* Change active locale based on browser language
* Install i18n to frontend
* Add Bootstrap 4 to frontend
* Add unit tests to frontend project
* Add license check to frontend
* Check frontend for code duplication
* Check commits for secrets accidentally committed
* Check frontend dependencies for security issues
* Setup fronted code linter
* Setup frontend continuous delivery
* Install Vue.js
* Set NodeJS version
* Create technical description files
* Set license
