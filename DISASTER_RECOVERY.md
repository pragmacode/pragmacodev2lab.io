# Disaster Recovery - PragmaCode Gitlab Page V2

## Data loss

TODO: configure backup

## Data leak

Once data has leaked we can only mitigate the damage:

* if the leak is still leaking
  - take down the application
  - fix the leak
  - bring back the application
* warn affected users about what has leaked
* force password change for affected users

## Access peak

TODO: deployment

## Denial of Service attack

* contact the infrastrucure provider and ask for support on mitigating the attack
* indentify if the attack comes from a limited set of IP addresses
  - if so, block them

