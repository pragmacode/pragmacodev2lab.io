# Hacking PragmaCode Gitlab Page V2

## System dependencies

* [Yarn](https://yarnpkg.com/getting-started/install)
* NodeJS
  - check [front/.nvmrc](front/.nvmrc) for the exact version
  - our preferred way of installation is using [NVM](https://github.com/nvm-sh/nvm#about)

## Configuration

TODO

## How to run the test suite

For frontend routines, read `front/README.md` first.

* Linter:
  - dependencies security risks
    * frontend `yarn audit`
  - security: `TODO`
  - for code style run
    * frontend `yarn lint`
  - for code duplications run
    * frontend `yarn jscpd`
  - dependencies license compatibility
    * install the tool `gem install license_finder` (you must have Ruby in your machine)
    * form each repository `license_finder`
* Unit tests:
  - frontend `yarn test:unit`

## Deployment instructions

Push to master and let Gitlab CI do the magic.
