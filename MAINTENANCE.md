# Maintenance - PragmaCode Gitlab Page V2

There follows a plan on what is the suggested periodicity in months for the different maintenance tasks for this application.

| Task                           | 01 | 02 | 03 | 04 | 05 | 06 | 07 | 08 | 09 | 10 | 11 | 12 |
| ------------------------------ | -- | -- | -- | -- | -- | -- | -- | -- | -- | -- | -- | -- |
| Check production logs          | *  | *  | *  |    |    | *  |    |    | *  |    |    | *  |
| Check server CPU and RAM usage | *  | *  | *  |    |    | *  |    |    | *  |    |    | *  |
| Run the entire pipeline        | *  | *  | *  | *  | *  | *  | *  | *  | *  | *  | *  | *  |
| Update programming lang        |    |    | *  |    |    | *  |    |    | *  |    |    | *  |
| Update packages                | *  | *  | *  | *  | *  | *  | *  | *  | *  | *  | *  | *  |
| Simulate disasters             |    |    |    |    |    | *  |    |    |    |    |    | *  |

After the 12th month follow the same periodicity.

## History

Write below a list in the format `* DD/MM/YYYY - What was done`.

* 04/05/2022 - Upgrade packages

    Still missing the following ones

    Package                            Current Wanted Latest Package Type    URL
    @fullhuman/postcss-purgecss        3.1.3   3.1.3  4.1.3  devDependencies https://purgecss.com
    @fullhuman/vue-cli-plugin-purgecss 3.1.3   3.1.3  4.1.0  devDependencies https://purgecss.com
    @vue/cli-plugin-babel              4.5.17  4.5.17 5.0.4  devDependencies https://github.com/vuejs/vue-cli/tree/dev/packages/@vue/cli-plugin-babel#readme
    @vue/cli-plugin-eslint             4.5.17  4.5.17 5.0.4  devDependencies https://github.com/vuejs/vue-cli/tree/dev/packages/@vue/cli-plugin-eslint#readme
    @vue/cli-service                   4.5.17  4.5.17 5.0.4  devDependencies https://cli.vuejs.org/
    bootstrap                          4.6.1   4.6.1  5.1.3  dependencies    https://getbootstrap.com/
    eslint                             7.32.0  7.32.0 8.14.0 devDependencies https://eslint.org
    eslint-config-prettier             7.2.0   7.2.0  8.5.0  devDependencies https://github.com/prettier/eslint-config-prettier#readme


* 27/11/2021 - Upgrade packages

    Still missing the floowing ones

    Package                            Current Wanted Latest Package Type    URL
    @fullhuman/postcss-purgecss        3.1.3   3.1.3  4.0.3  devDependencies https://purgecss.com
    @fullhuman/vue-cli-plugin-purgecss 3.1.3   3.1.3  4.0.3  devDependencies https://purgecss.com
    bootstrap                          4.6.1   4.6.1  5.1.3  dependencies    https://getbootstrap.com/
    eslint                             7.32.0  7.32.0 8.3.0  devDependencies https://eslint.org
    eslint-config-prettier             7.2.0   7.2.0  8.3.0  devDependencies https://github.com/prettier/eslint-config-prettier#readme
