# PragmaCode V2 Gitlab.Io

Version 2 of PragmaCode Website. Features the new brand, internationalization and contact form. Check the old at https://gitlab.com/pragmacode/pragmacode.gitlab.io/

You can access this version in https://pragmacodev2.gitlab.io

## Technical aspects

* On how to get started developing, check the [HACKING](HACKING.md) file
* On how to keep the application up and running, check the [MAINTENANCE](MAINTENANCE.md) file
* On how to recover from disasters, check the [DISASTER_RECOVERY](DISASTER_RECOVERY.md) file

## License

[Copyright - All rights reserved to PragmaCode](COPYING)


### CC BY 3.0 Dependencies

* [spdx-exceptions](https://github.com/jslicense/spdx-exceptions.json#readme)

### CC BY 4.0 Dependencies

* [caniuse-lite](https://github.com/ben-eb/caniuse-lite/)
* [fortawesome/free-solid-svg-icons](https://github.com/FortAwesome/Font-Awesome/tree/master/js-packages/%40fortawesome/free-solid-svg-icons)
* [fortawesome/free-brands-svg-icons](https://github.com/FortAwesome/Font-Awesome/tree/master/js-packages/%40fortawesome/free-brands-svg-icons)
