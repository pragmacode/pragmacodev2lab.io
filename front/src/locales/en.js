export default {
  hello: "Hello",
  components: {
    navbar: {
      about: "about",
      services: "services",
      projects: "projects",
      team: "our team",
      contact: "contact",
    },
    cover: {
      concept_phrase_i: "We build software",
      concept_phrase_ii: "We deliver confidence and flexibility",
    },
    about: {
      title: "about us",
      description_i:
        "We are a consulting company in technological product development: we build software and deliver digital experiences, security, and flexibility to businesses of all sizes and sectors.",
      description_ii:
        "We provide a simple, safe, and efficient interaction between companies and their products, helping them to prosper in an increasingly technological world.",
    },
    principles: {
      excellence_title: "excellence",
      excellence_text: "We deliver practical and quality features.",
      commitment_title: "commitment",
      commitment_text:
        "We are engaged and fully involved in each project to honor deliveries and exceed our customers' expectations.",
      colab_work_title: "collaborative work",
      colab_work_text:
        "We discover with our customers the best technological solutions for their products.",
    },
    services: {
      title: "services",
      description:
        "We offer a complete package for our customers' software, supporting them from conception and development to deployment on servers, stability monitoring and further developments.",
      development: "development",
      prototyping: "prototyping",
      maintenance: "maintenance",
    },
    customers: {
      title: "who we've helped",
    },
    cases: {
      title: "featured projects",
      pae: "Plataforma de apoio ao empresário",
      vetor: "Updating of people management platform to act in governments",
      interscity: "State-of-the-art software system for smart cities",
    },
    caseDescriptionSection: {
      challenge: {
        title: "Challenge",
        pae: "Need for quality information and assistance on better legal options for companies in financial crisis, in the difficult context of the pandemic.",
        interscity:
          "Create stable and maintainable software that can be used on a large scale and made available for use by society.",
        vetor: `Stop using some third party systems and implement new features within the Selection Platform itself;
        <br /> <br />
        Assist in the refinement and development of new products.`,
      },
      results: {
        title: "Results",
        pae: `A platform - developed adapted for viewing on different screen sizes - that brings:
          <br /> <br />
          Complete and simple content management that allows you to always provide the latest information. Automation of processes that required a lot of time from all parties involved.`,
        interscity:
          "Installation of the InterSCity Platform with 99.99% availability within 30 days. New version of the HealthDashboard platform delivered and receiving feedback from its users for improvements. During the pandemic, the InterSCity Platform was used to receive and store Sao Paulo traffic data for computing social isolation metrics. In HealthDashboard we made it possible to load on the map all 500 thousand hospitalizations in the public network of the city of São Paulo in 2015.",
        vetor:
          "Rewriting and launching Compasso, with additional features that the previous system did not include, such as integration with the Vetor Brasil recommendation system. Review of the infrastructure of Vetor Brasil systems, with significant reduction in costs for the organization. Mentoring of interns to distribute knowledge and develop an internal team.",
      },
      solution: {
        title: "Solution",
        pae: "Creation of a platform with content management and document automation in a simple and direct way, different from those that already exist in the market for this purpose.",
        interscity:
          "Deploy continuous delivery pipelines in the projects together with infrastructure automation; create a new version of an existing software, with the same functionalities, but already prepared to be able to absorb the new demands of users more easily.",
        vetor:
          "Removing ForestAdmin from the Selection Platform and implementing the program's actions on the platform itself. Refinement and rewriting of a new product called Compasso. Infrastructure management. Leadership, maintenance and development of all platform systems. Participation in the design of new products.",
      },
    },
    caseDescriptionTechnologies: {
      title: "We use state-of-the-art technology:",
    },
    team: {
      title: "our team",
    },
    teamMember: {
      diego: {
        name: "Diego Camarinha",
        bio: "Programmer since 2007, he has a bachelor's and master's degree in computer science from IME - USP. He has experience in development projects and performance analysis of web systems.",
      },
      rafael: {
        name: "Rafael Manzo",
        bio: "He started programming in 2008 and today he has a bachelor's and master's degree in computer science from IME - USP. He works on projects ranging from online stores to processing magnetic resonance images.",
      },
      eduardo: {
        name: "Eduardo Araújo",
        bio: "Graduating in Physics at the University of São Paulo. He has experience in web projects, game development, process automation and contributions to free software projects.",
      },
      heitor: {
        name: "Heitor Reis",
        bio: "Bachelor of Computer Science from IME - USP. Acts in the development of web applications with a focus on Rails, and probabilistic models for machine learning (Sum-Product Networks).",
      },
      andre: {
        name: "André Silveira",
        bio: "Playing with excel spreadsheets simulating championships since 10 years old, but only really started programming when he entered the computer science course in 2014. He discovered his passion for web development 2 years later when he met Rails, and today he is a great enthusiast in the Typescript community.",
      },
      pedro: {
        name: "Pedro Ligabue",
        bio: "BSc in Electrical Engineering with emphasis on computing from POLI-USP. Works with web development, but also enjoys functional programming and ML.",
      },
      wilson: {
        name: "Wilson Mizutani",
        bio: "PhD in computer science, specializes in software architecture. As a co-founder of USPGameDev, led multiple projects using a variety of open-source technologies. His main programming languages are C++ and Lua.",
      },
      arthur: {
        name: "Arthur Vieira",
        bio: "Started programming in 2009 because of his passion for developing games, which led him to pursue a bachelor's degree in Computer Science from IME - USP. Also got in touch with open source technologies from game development and is now a great proponent of using them whenever possible.",
      },
      isaque: {
        name: "Isaque Alves",
        bio: "Software Engineer at UnB and Master's in Computer Science at IME-USP. He started his studies at IT in 2011 and today focuses on researching the DevOps Teams Topologies and Product Engineering for Machine Learning.",
      },
      rubens: {
        name: "Rubens Gomes",
        bio: "Developed their interest in programming while studying Astronomy, and now has a bachelor's degree in Computer Science from IME-USP. During their time in university they worked on Free Software projects relating to Linux kernel development.",
      },
    },
    contact: {
      title: "contact",
      description:
        "Want to know more? <br /> Send an email to {email} or leave us a message below. We will love to help you!",
      submit: "send",
    },
    contactForm: {
      name: "name",
      email: "email",
      message: "message",
    },
    navbarLandingPage: {
      contact: "Request evaluation",
    },
    landingPageSection: {
      independent: {
        title: "Independent analysis",
        description_i:
          "However good the professionals working on your software are, there is an attachment for their own work",
        description_ii:
          "We make an evaluation completely unbiased, provinding information about the real state of your software. That way you and your investors will have more confidence about your product.",
      },
      experts: {
        title: "Evaluated by experts",
        description_i:
          "PragmaCode has an experient and skilled development team that will assess in a customized way your product.",
        description_ii: "",
      },
      confidential: {
        title: "Protected intellectual property",
        description_i:
          "To produce the technical advice we need to study your software code. We commit ourselves to encrypt all received material and to sign a confidentiality agreement to guarantee our commitment.",
        description_ii: "",
      },
    },
  },
  views: {
    index: {
      title: "PragmaCode - Product Development",
    },
    "service-title": {
      prototyping: "PragmaCode - Prototyping Services",
      development: "PragmaCode - Development Services",
      maintenance: "PragmaCode - Maintenance Services",
    },
    "service-description": {
      prototyping:
        "Together with our customers, we developed prototypes (also known as MVP - Minimal Viable Product), so that your ideas can be validated quickly and at low cost.",
      development:
        "We develop the product collaboratively with the customer and through constant deliveries, providing a continuous view of the work performed. Thus, our customers perceive the return on their investments from the beginning to the end of the project.",
      maintenance:
        "We offer our customers the necessary maintenance to guarantee the correction of any failure found. We keep the system up to date and continuously monitor its operation to ensure that everything is running smoothly.",
    },
    "page-not-found": {
      error: "The page you are looking for doesn't exist",
      click: "Click",
      here: "here",
      message: "to go to the homepage",
    },
    "case-description": {
      pae: "PAE",
      vetor: "VETOR BRASIL",
      interscity: "INTERSCITY",
    },
    "contact-response": {
      success:
        "Your contact has been sent successfully. We will be back to you soon!",
      failed:
        "There was a problem sending your contact. Please try again. If the problem persists, please send an email to {email}",
      click: "Click",
      here: "here",
      message: "to go back to the homepage",
    },
    "technical-advice": {
      title: "Do you need a technical advice for your software?",
      subtitle: "We can help you!",
      description:
        "Software is one of the biggest investments companies make nowadays. Therefore, it's important to assess the quality of the delivered system in an effective way.",
      contactDescription: "Fill the form below and request your evaluation!",
      contact: "Request evaluation",
      footerContactText:
        "Discover your software true state and protect your product",
      footerContactButton: "Request evaluation",
    },
  },
};
