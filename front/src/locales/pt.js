export default {
  hello: "Oi",
  components: {
    navbar: {
      about: "sobre",
      services: "serviços",
      projects: "projetos",
      team: "nosso time",
      contact: "contato",
    },
    cover: {
      concept_phrase_i: "Construímos software",
      concept_phrase_ii: "Entregamos confiança e flexibilidade",
    },
    about: {
      title: "sobre nós",
      description_i:
        "Somos uma empresa de consultoria em desenvolvimento de produtos tecnológicos: construímos software e entregamos experiências digitais, segurança e flexibilidade a negócios de todos os portes e setores.",
      description_ii:
        "Proporcionamos uma interação simples, segura e eficiente das empresas com os seus produtos, ajudando-as a prosperarem em um mundo cada vez mais tecnológico.",
    },
    principles: {
      excellence_title: "excelência",
      excellence_text: "Entregamos funcionalidades práticas e de qualidade.",
      commitment_title: "comprometimento",
      commitment_text:
        "Nos engajamos e nos envolvemos ao máximo em cada projeto para honrar as entregas e superar às expectativas dos nossos clientes.",
      colab_work_title: "trabalho colaborativo",
      colab_work_text:
        "Descobrimos junto com os nossos clientes as melhores soluções tecnológicas para os seus produtos.",
    },
    services: {
      title: "serviços",
      description:
        "Oferecemos um pacote completo para o software dos nossos clientes, apoiando-os desde a concepção e desenvolvimento até a implantação nos servidores, monitoramento da estabilidade e evoluções posteriores.",
      development: "desenvolvimento",
      prototyping: "criação de protótipos",
      maintenance: "manutenção",
    },
    customers: {
      title: "quem já ajudamos",
    },
    cases: {
      title: "projetos de destaque",
      pae: "Plataforma de apoio ao empresário",
      vetor:
        "Atualização de plataforma de gestão de pessoas para atuação em governos",
      interscity:
        "Sistema de software com tecnologia de ponta para cidades inteligentes",
    },
    caseDescriptionSection: {
      challenge: {
        title: "Desafio",
        pae: "Necessidade de informação de qualidade e auxílio sobre melhores opções jurídicas para empresas em crise financeira, no contexto difícil da pandemia.",
        interscity:
          "Criar software estável e manutenível que possa ser usado em larga escala e disponibilizado para uso da sociedade.",
        vetor: `Interromper o uso de alguns sistemas terceiros e implementar novas funcionalidades dentro da própria Plataforma de Seleção;
        <br /> <br />
        Auxiliar no refinamento e desenvolvimento de novos produtos.`,
      },
      results: {
        title: "Resultados",
        pae: `Uma plataforma - desenvolvida adaptada para visualização em diferentes tamanhos de tela - que traz:
          <br /> <br />
          Gerenciamento completo e simples de conteúdos que permite sempre fornecer as informações mais recentes. Automatização de processos que exigiam muito tempo de todas as partes envolvidas.`,
        interscity:
          "Instalação da Plataforma InterSCity com 99,99% de disponibilidade no período de 30 dias. Nova versão da plataforma HealthDashboard entregue e recebendo feedback de seus usuários para melhorias. Durante a pandemia a Plataforma InterSCity foi utilizada para receber e armazenar dados de trânsito da cidade de São Paulo para computação de métricas de isolamento social. No HealthDashboard tornamos possível carregar no mapa todas as 500 mil internações na rede pública da cidade de São Paulo do ano de 2015.",
        vetor:
          "Reescrita e lançamento do Compasso, já com funcionalidades adicionais que o sistema anterior não contemplava, como a integração com o sistema de recomendação do Vetor Brasil. Revisão da infraestrutura dos sistemas do Vetor Brasil, com redução significativa nos custos para a organização. Mentoria de estagiários para distribuição de conhecimento e desenvolvimento de uma equipe interna.",
      },
      solution: {
        title: "Solução",
        pae: "Criação de plataforma com gerenciamento de conteúdos e automatização de documentos de forma simples e direta, diferente das que já existem no mercado para este fim.",
        interscity:
          "Implantar nos projetos esteiras de entrega contínua junto com automação de infraestrutura; criar uma nova versão de um software já existente, com as mesmas funcionalidades, mas já preparada para ser possível absorver as novas demandas dos usuários com maior facilidade.",
        vetor:
          "Remoção do ForestAdmin da Plataforma de Seleção e implementação das ações do programa na própria plataforma. Refinamento e reescrita de um novo produto chamado Compasso. Gerenciamento da infraestrutura. Liderança, manutenção e desenvolvimento de todos os sistemas da plataforma. Participação na concepção de novos produtos.",
      },
    },
    caseDescriptionTechnologies: {
      title: "Utilizamos o que há de mais moderno na tecnologia:",
    },
    team: {
      title: "nosso time",
    },
    teamMember: {
      diego: {
        name: "Diego Camarinha",
        bio: "Programador desde 2007, possui bacharelado e mestrado em ciência da computação pelo IME - USP. Tem experiência em projetos de desenvolvimento e análise de desempenho de sistemas web.",
      },
      rafael: {
        name: "Rafael Manzo",
        bio: "Começou a programar em 2008 e hoje é bacharel e mestre em ciência da computação pelo IME - USP. Atua em projetos que vão desde lojas online até processamento de imagens de ressonância magnética.",
      },
      eduardo: {
        name: "Eduardo Araújo",
        bio: "Graduando em Física pela Universidade de São Paulo. Tem experiência em projetos web, desenvolvimento de jogos, automação de processos e contribuições em projetos de software livre.",
      },
      heitor: {
        name: "Heitor Reis",
        bio: "Bacharel em Ciência da Computação pelo IME - USP. Atua no desenvolvimento de aplicativos web com foco em Rails, e modelos probabilísticos para aprendizagem de máquina (Sum-Product Networks).",
      },
      andre: {
        name: "André Silveira",
        bio: "Fazia planilhas de excel simulando campeonatos desde os 10 anos, mas só começou a programar de fato quando entrou na faculdade de ciência da computação em 2014. Descobriu sua paixão por desenvolvimento web 2 anos depois ao conhecer o Rails, e hoje é um grande entusiasta na comunidade Typescript.",
      },
      pedro: {
        name: "Pedro Ligabue",
        bio: "Bacharel em Engeharia Elétrica com ênfase em computação pela POLI-USP. Trabalha com desenvolvimento web, mas também gosta de programação funcional e ML.",
      },
      wilson: {
        name: "Wilson Mizutani",
        bio: "Doutor em Ciênciada Computação especialista em arquitetura de software. Como co-fundador do USPGameDev, liderou múltiplos projetos com tecnologias de código aberto. Suas principais linguagens de programação são C++ e Lua.",
      },
      arthur: {
        name: "Arthur Vieira",
        bio: "Começou a programar em 2009 por causa de sua paixão por desenvolver jogos, que o levou a fazer bacharelado em ciência da computação pelo IME - USP. Também teve seu primeiro contato com tecnologias de software livre a partir do desenvolvimento de jogos e agora é um adepto de usá-las sempre que possível.",
      },
      isaque: {
        name: "Isaque Alves",
        bio: "Engenheiro de Software pela UnB e Mestrando em Ciências da Computação pelo IME-USP. Iniciou os estudos na TI em 2011 e hoje foca em pesquisar a Topologia dos times DevOps e aspectos da Engenharia de Produto no desenvolvimento de Machine Learning.",
      },
      rubens: {
        name: "Rubens Gomes",
        bio: "Descobriu o interesse por programação durante o curso de Astronomia, e hoje é formado em Ciência da Computação pelo IME-USP. Durante a graduação trabalhou em projetos de Software Livre relacionados ao desenvolvimento do kernel Linux.",
      },
    },
    contact: {
      title: "contato",
      description:
        "Quer saber mais? <br /> Envie um e-mail para {email} ou deixe uma mensagem abaixo. Vamos adorar ajudar você!",
      submit: "enviar",
    },
    contactForm: {
      name: "nome",
      email: "e-mail",
      message: "mensagem",
    },
    navbarLandingPage: {
      contact: "Solicitar avaliação",
    },
    landingPageSection: {
      independent: {
        title: "Análise independente",
        description_i:
          "Por melhor que seja o profissional desenvolvendo o seu software, sabemos que existe um apego pelo próprio trabalho",
        description_ii:
          "Realizamos uma avaliação totalmente imparcial, fornecendo informações sobre o real estado do seu software, gerando assim mais confiança no seu produto.",
      },
      experts: {
        title: "Produzido por especialistas",
        description_i:
          "Contamos com um time de desenvolvedores experientes e qualificados, que vão avaliar de forma personalizada o seu produto.",
        description_ii: "",
      },
      confidential: {
        title: "Sua propriedade intelectual protegida",
        description_i:
          "Para fornecer o parecer técnico, precisamos estudar o seu software. Temos o compromisso de armazenar todo o material recebido de forma encriptada, além de assinarmos um termo de confiabilidade para garantir o sigilo e proteção.",
        description_ii: "",
      },
    },
  },
  views: {
    index: {
      title: "PragmaCode - Desenvolvimento de Produtos",
    },
    "service-title": {
      prototyping: "PragmaCode - Serviços de Prototipagem",
      development: "PragmaCode - Serviços de Desenvolvimento",
      maintanance: "PragmaCode - Serviços de Manutenção",
    },
    "service-description": {
      prototyping:
        "Junto com os nossos nossos clientes desenvolvemos os protótipos (também conhecidos como MVP - Minimal Viable Product), para que suas ideias sejam validadas de forma rápida e com baixo custo.",
      development:
        "Desenvolvemos o produto de forma colaborativa com o cliente e através de entregas constantes, fornecendo uma visão contínua do trabalho realizado. Assim, nossos clientes percebem o retorno de seus investimentos do início ao fim do projeto.",
      maintenance:
        "Oferecemos a nossos clientes a manutenção necessária para garantir a correção de qualquer falha encontrada. Mantemos o sistema atualizado e continuamente monitoramos seu funcionamento para garantir que tudo está sendo executado da forma correta.",
    },
    "page-not-found": {
      error: "A página que você procura não existe",
      click: "Clique",
      here: "aqui",
      message: "para ir para a página principal",
    },
    "case-description": {
      pae: "PAE",
      vetor: "VETOR BRASIL",
      interscity: "INTERSCITY",
    },
    "contact-response": {
      success:
        "Seu contato foi enviado com sucesso. Em breve retornamos no e-mail indicado!",
      failed:
        "Houve algum problema no envio do seu contato. Por favor, tente novamente. Se o problema persistir, por favor envie um e-mail para {email}",
      click: "Clique",
      here: "aqui",
      message: "para voltar à página principal",
    },
    "technical-advice": {
      title: "Precisa de um parecer técnico sobre o seu software?",
      subtitle: "A gente pode te ajudar!",
      description:
        "O investimento em software é um dos maiores feitos pelas empresas nos dias de hoje. Por isso, é muito importante que a qualidade do sistema entregue seja avaliada de forma efetiva.",
      contactDescription:
        "Preencha o formulário abaixo e solicite sua avaliação!",
      contact: "Solicitar avaliação",
      footerContactText:
        "Descubra o real estado do seu software e proteja seu produto",
      footerContactButton: "Solicitar avaliação",
    },
  },
};
