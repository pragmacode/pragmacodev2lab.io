import { createApp } from "vue";
import App from "./App.vue";

import { createI18n } from "vue-i18n";
import pt from "./locales/pt";
import en from "./locales/en";

import router from "./router";

import vueHeadful from "vue-headful";

function startLocale() {
  const navigatorLocale =
    navigator.languages !== undefined
      ? navigator.languages[0]
      : navigator.language;

  if (!navigatorLocale) {
    return "pt";
  }

  return navigatorLocale.trim().split(/[-_]/)[0];
}

const i18n = createI18n({
  locale: startLocale(),
  fallbackLocale: "en",
  messages: {
    pt,
    en,
  },
});

const app = createApp(App);

app.use(i18n);
app.use(router);
app.component("vue-headful", vueHeadful);

app.mount("#app");
