import { shallowMount } from "@vue/test-utils";

jest.mock("vue-i18n", () => ({
  __esModule: true, // this property makes it work
  useI18n: jest.fn(),
}));
import { useI18n } from "vue-i18n";

import router from "@/router";
import App from "@/App.vue";

describe("App view test", () => {
  let wrapper;

  beforeEach(() => {
    wrapper = shallowMount(App, {
      global: {
        plugins: [router],
      },
    });
  });

  it("is expected to render", () => {
    expect(wrapper).toBeDefined();
  });

  it("is expected to initialize the i18n", () => {
    expect(useI18n).toHaveBeenCalled();
  });
});
