import { shallowMount } from "@vue/test-utils";

import AboutSection from "@/components/AboutSection.vue";

describe("AboutSection view test", () => {
  let wrapper;
  let $t;
  const translatedText = "text in another language";

  beforeEach(() => {
    $t = jest.fn().mockReturnValue(translatedText);

    wrapper = shallowMount(AboutSection, {
      global: {
        mocks: {
          $t,
        },
      },
    });
  });

  it("is expected to render", () => {
    expect(wrapper).toBeDefined();
  });

  it("is expected to render the title", () => {
    expect($t).toHaveBeenCalledWith("components.about.title");

    const titleParagraph = wrapper.find("p.about-title");
    expect(titleParagraph.text()).toContain(translatedText);
  });

  it("is expected to render the description first paragraph", () => {
    expect($t).toHaveBeenCalledWith("components.about.description_i");

    const descriptionParagraph = wrapper.find("p.about-description");
    expect(descriptionParagraph.text()).toContain(translatedText);
  });

  it("is expected to render the description second paragraph", () => {
    expect($t).toHaveBeenCalledWith("components.about.description_ii");

    const descriptionParagraph = wrapper.find("p.about-description");
    expect(descriptionParagraph.text()).toContain(translatedText);
  });
});
