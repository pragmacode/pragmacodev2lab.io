/* jscpd:ignore-start */
import { shallowMount } from "@vue/test-utils";

import router from "@/router";

import CaseDescriptionSection from "@/components/CaseDescriptionSection.vue";

describe("CaseDescriptionSection view test", () => {
  let wrapper;
  let $t;
  let $tm;
  const translatedText = "text in another language";
  const name = "pae";
  const kind = "results";

  beforeEach(() => {
    $t = jest.fn().mockReturnValue(translatedText);
    $tm = jest.fn().mockReturnValue(translatedText);
    wrapper = shallowMount(CaseDescriptionSection, {
      global: {
        mocks: {
          $t,
          $tm,
        },
        plugins: [router],
      },
      props: {
        name,
        kind,
      },
    });
  });

  it("is expected to render", () => {
    expect(wrapper).toBeDefined();
  });

  it("is expected to render the subtitle", () => {
    expect($t).toHaveBeenCalledWith(
      `components.caseDescriptionSection.${kind}.title`
    );

    const titleParagraph = wrapper.find("div.case-description-subtitle");
    expect(titleParagraph.text()).toContain(translatedText);
  });

  it("is expected to render the description", () => {
    expect($tm).toHaveBeenCalledWith(
      `components.caseDescriptionSection.${kind}.${name}`
    );
  });
});
/* jscpd:ignore-end */
