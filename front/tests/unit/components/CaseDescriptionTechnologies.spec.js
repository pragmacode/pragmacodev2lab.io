/* jscpd:ignore-start */
import { shallowMount } from "@vue/test-utils";

import CaseDescriptionTechnologies from "@/components/CaseDescriptionTechnologies.vue";

describe("CaseDescriptionTechnologies view test", () => {
  let wrapper;
  let $t;
  const translatedText = "text in another language";
  const name = "pae";

  beforeEach(() => {
    $t = jest.fn().mockReturnValue(translatedText);
    wrapper = shallowMount(CaseDescriptionTechnologies, {
      global: {
        mocks: {
          $t,
        },
      },
      props: {
        name,
      },
    });
  });

  it("is expected to render", () => {
    expect(wrapper).toBeDefined();
  });

  it("is expected to render the subtitle", () => {
    expect($t).toHaveBeenCalledWith(
      "components.caseDescriptionTechnologies.title"
    );

    const titleParagraph = wrapper.find(
      "div.case-description-technologies-title"
    );
    expect(titleParagraph.text()).toContain(translatedText);
  });
});
/* jscpd:ignore-end */
