/* jscpd:ignore-start */
import { shallowMount } from "@vue/test-utils";

import router from "@/router";

import CaseItem from "@/components/CaseItem.vue";

describe("CaseItem view test", () => {
  let wrapper;
  let $t;
  const translatedText = "text in another language";
  const name = "pae";

  beforeEach(() => {
    $t = jest.fn().mockReturnValue(translatedText);
    wrapper = shallowMount(CaseItem, {
      global: {
        mocks: {
          $t,
        },
        plugins: [router],
      },
      props: {
        name,
      },
    });
  });

  it("is expected to render", () => {
    expect(wrapper).toBeDefined();
  });

  it("is expected to render the image", () => {
    const webp = wrapper.find("picture source");
    const fallbackPng = wrapper.find("picture img");

    expect(webp.exists()).toBe(true);
    expect(fallbackPng.exists()).toBe(true);
  });

  it("is expected to render the title", () => {
    const titleParagraph = wrapper.find("div.case-title");
    expect(titleParagraph.exists()).toBe(true);
  });
});
/* jscpd:ignore-end */
