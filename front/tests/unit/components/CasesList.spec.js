import { shallowMount } from "@vue/test-utils";

import CasesList from "@/components/CasesList.vue";

describe("CasesList view test", () => {
  let wrapper;
  let $t;
  const translatedText = "text in another language";

  beforeEach(() => {
    $t = jest.fn().mockReturnValue(translatedText);
    wrapper = shallowMount(CasesList, {
      global: {
        mocks: {
          $t,
        },
      },
    });
  });

  it("is expected to render", () => {
    expect(wrapper).toBeDefined();
  });

  it("is expected to render the title", () => {
    expect($t).toHaveBeenCalledWith("components.cases.title");

    const titleParagraph = wrapper.find("p.title");
    expect(titleParagraph.text()).toContain(translatedText);
  });

  it("is expected to renders the Customer component", () => {
    const customer = wrapper.find("case-item-stub");
    expect(customer.exists()).toBe(true);
  });
});
