jest.mock("axios");
import axios from "axios";

import { shallowMount } from "@vue/test-utils";

import router from "@/router";

import ContactForm from "@/components/ContactForm.vue";

describe("ContactForm view test", () => {
  let wrapper;
  let $t;
  const translatedText = "text in another language";

  beforeEach(() => {
    $t = jest.fn().mockReturnValue(translatedText);

    wrapper = shallowMount(ContactForm, {
      global: {
        mocks: {
          $t,
        },
        plugins: [router],
      },
    });
  });

  it("is expected to render", () => {
    expect(wrapper).toBeDefined();
  });

  it("is expected to render the name input", () => {
    const nameInput = wrapper.find("input#name");
    expect(nameInput.exists()).toBe(true);
  });

  it("is expected to render the email input", () => {
    const emailInput = wrapper.find("input#email");
    expect(emailInput.exists()).toBe(true);
  });

  it("is expected to render the message textarea", () => {
    const messageTextarea = wrapper.find("textarea#message");
    expect(messageTextarea.exists()).toBe(true);
  });

  describe("method", () => {
    describe("handleSubmit", () => {
      const expectedAxiosConfig = {
        header: { "Content-Type": "application/x-www-form-urlencoded" },
      };
      const encodedParams = "encodedParams";
      const expectEncoded = () => {
        expect(wrapper.vm.encode).toHaveBeenCalledWith({
          "form-name": "contact",
          ...wrapper.vm.form,
        });
      };
      const expectAxios = () => {
        expect(axios.post).toHaveBeenCalledWith(
          "/",
          encodedParams,
          expectedAxiosConfig
        );
      };

      afterEach(() => {
        jest.resetAllMocks();
      });

      describe("with a successfull response", () => {
        beforeEach(() => {
          jest.spyOn(axios, "post").mockReturnValue(Promise.resolve());
          wrapper.vm.encode = jest.fn().mockReturnValue(encodedParams);
          jest.spyOn(router, "push");

          wrapper.vm.handleSubmit();
        });

        it("is expected to encode the form params", expectEncoded);

        it("is epxcted to post the form using axios", expectAxios);

        it("is expected to navigate to the successfull contact response page", () => {
          expect(router.push).toHaveBeenCalledWith({
            name: "SuccessfulContact",
          });
        });
      });

      describe("with a failed response", () => {
        beforeEach(() => {
          jest.spyOn(axios, "post").mockReturnValue(Promise.reject());
          wrapper.vm.encode = jest.fn().mockReturnValue(encodedParams);
          jest.spyOn(router, "push");

          wrapper.vm.handleSubmit();
        });

        it("is expected to encode the form params", expectEncoded);

        it("is epxcted to post the form using axios", expectAxios);

        it("is expected to navigate to the failed contact response page", () => {
          expect(router.push).toHaveBeenCalledWith({ name: "FailedContact" });
        });
      });
    });

    describe("encode", () => {
      const data = {
        param: "value",
      };
      let returnedValue;

      beforeEach(() => {
        returnedValue = wrapper.vm.encode(data);
      });

      it("is expected to return the data in x-www-form-urlencoded", () => {
        expect(returnedValue).toEqual("param=value");
      });
    });
  });
});
