import { shallowMount } from "@vue/test-utils";

import ContactSection from "@/components/ContactSection.vue";

describe("ContactSection view test", () => {
  let wrapper;
  let $t;
  const translatedText = "text in another language";

  beforeEach(() => {
    $t = jest.fn().mockReturnValue(translatedText);

    wrapper = shallowMount(ContactSection, {
      global: {
        mocks: {
          $t,
        },
      },
    });
  });

  it("is expected to render", () => {
    expect(wrapper).toBeDefined();
  });

  it("is expected to render the title", () => {
    expect($t).toHaveBeenCalledWith("components.contact.title");

    const titleParagraph = wrapper.find("p.title");
    expect(titleParagraph.text()).toContain(translatedText);
  });

  it("is expected to renders the ContactForm component", () => {
    const contactForm = wrapper.find("contact-form-stub");
    expect(contactForm.exists()).toBe(true);
  });
});
