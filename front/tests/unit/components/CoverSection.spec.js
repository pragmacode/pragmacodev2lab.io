import { shallowMount } from "@vue/test-utils";

import CoverSection from "@/components/CoverSection.vue";

describe("CoverSection view test", () => {
  let wrapper;
  let $t;
  const translatedText = "text in another language";

  beforeEach(() => {
    $t = jest.fn().mockReturnValue(translatedText);

    wrapper = shallowMount(CoverSection, {
      global: {
        mocks: {
          $t,
        },
      },
    });
  });

  it("is expected to render", () => {
    expect(wrapper).toBeDefined();
  });

  it("is expected to render the concept_phrase", () => {
    expect($t).toHaveBeenCalledWith("components.cover.concept_phrase_i");
    expect($t).toHaveBeenCalledWith("components.cover.concept_phrase_ii");

    const textDiv = wrapper.find("h1.cover-text");

    expect(textDiv.text()).toContain(translatedText);
  });

  it("is expected to render the cover image", () => {
    const webp = wrapper.find("picture source.cover");
    const fallbackPng = wrapper.find("picture img.cover");

    expect(webp.exists()).toBe(true);
    expect(fallbackPng.exists()).toBe(true);
  });

  it("is expected to render the squares image", () => {
    const webp = wrapper.find("picture source.squares");
    const fallbackPng = wrapper.find("picture img.squares");

    expect(webp.exists()).toBe(true);
    expect(fallbackPng.exists()).toBe(true);
  });
});
