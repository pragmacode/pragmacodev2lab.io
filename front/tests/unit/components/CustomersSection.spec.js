import { shallowMount } from "@vue/test-utils";

import CustomersSection from "@/components/CustomersSection.vue";

describe("CustomersSection view test", () => {
  let wrapper;
  let $t;
  const translatedText = "text in another language";

  beforeEach(() => {
    $t = jest.fn().mockReturnValue(translatedText);
    wrapper = shallowMount(CustomersSection, {
      global: {
        mocks: {
          $t,
        },
      },
    });
  });

  it("is expected to render", () => {
    expect(wrapper).toBeDefined();
  });

  it("is expected to render the title", () => {
    expect($t).toHaveBeenCalledWith("components.customers.title");

    const titleParagraph = wrapper.find("p.title");
    expect(titleParagraph.text()).toContain(translatedText);
  });

  it("is expected to renders the CustomerItem component", () => {
    const customer = wrapper.find("customer-item-stub");
    expect(customer.exists()).toBe(true);
  });
});
