import { shallowMount } from "@vue/test-utils";

import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome";
import IconItem from "@/components/IconItem.vue";

describe("IconItem view test", () => {
  let wrapper;

  const icon_type = "external";
  const size = "xs";

  beforeEach(() => {
    wrapper = shallowMount(IconItem, {
      global: {
        components: {
          FontAwesomeIcon,
        },
      },
      props: {
        icon_type,
        size,
      },
    });
  });

  it("is expected to render", () => {
    expect(wrapper).toBeDefined();
  });

  it("is expected to render the icons", () => {
    const icon = wrapper.find("font-awesome-icon-stub");
    expect(icon.exists()).toBe(true);
  });
});
