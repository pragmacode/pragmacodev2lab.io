import { shallowMount } from "@vue/test-utils";

import LandingPageSection from "@/components/LandingPageSection.vue";

describe("LandingPageSection view test", () => {
  let wrapper;
  let $t;

  const translatedText = "text in another language";
  const topic = "section";
  const imgOrder = "first";
  const bgColor = "white";

  beforeEach(() => {
    $t = jest.fn().mockReturnValue(translatedText);

    wrapper = shallowMount(LandingPageSection, {
      global: {
        mocks: {
          $t,
        },
      },
      props: {
        topic,
        imgOrder,
        bgColor,
      },
    });
  });

  it("is expected to render", () => {
    expect(wrapper).toBeDefined();
  });

  it("is expected to render the image", () => {
    const webp = wrapper.find("picture source");
    const fallbackPng = wrapper.find("picture img");

    expect(webp.exists()).toBe(true);
    expect(fallbackPng.exists()).toBe(true);
  });

  it("is expected to render the subtitle", () => {
    expect($t).toHaveBeenCalledWith(
      `components.landingPageSection.${topic}.title`
    );

    const element = wrapper.find("h2.title");
    expect(element.exists()).toBe(true);
    expect(element.text()).toContain(translatedText);
  });

  it("is expected to render the description items", () => {
    expect($t).toHaveBeenCalledWith(
      `components.landingPageSection.${topic}.description_i`
    );
    expect($t).toHaveBeenCalledWith(
      `components.landingPageSection.${topic}.description_ii`
    );

    const element = wrapper.find("p.text-justify");
    expect(element.exists()).toBe(true);
    expect(element.text()).toContain(translatedText);
  });
});
