import { shallowMount } from "@vue/test-utils";

import NavbarLandingPage from "@/components/NavbarLandingPage.vue";

describe("NavbarLandingPage view test", () => {
  let wrapper;
  let $router;
  let $t;
  const navClass = "work";
  const logo = "logo";
  const indexSection = "#workers";
  const translatedText = "text in another language";

  beforeEach(() => {
    $t = jest.fn().mockReturnValue(translatedText);

    $router = {
      push: jest.fn(),
    };

    wrapper = shallowMount(NavbarLandingPage, {
      props: {
        navClass,
        logo,
        indexSection,
      },
      global: {
        mocks: {
          $router,
          $t,
        },
      },
    });
  });

  it("is expected to render", () => {
    expect(wrapper).toBeDefined();
  });

  describe("page elements", () => {
    it("is expected to render the logo", () => {
      const navbarLogo = wrapper.find("navbar-logo-stub");

      expect(navbarLogo.exists()).toBe(true);
    });

    it("is expected to render the contact link", () => {
      const link = wrapper.find("a.contact-link");
      expect(link.exists()).toBe(true);
    });

    it("is expected to translate the contact link text", () => {
      expect($t).toHaveBeenCalledWith("components.navbarLandingPage.contact");
    });
  });
});
