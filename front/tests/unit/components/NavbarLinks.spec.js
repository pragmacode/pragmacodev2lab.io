import { shallowMount } from "@vue/test-utils";

import NavbarLinks from "@/components/NavbarLinks.vue";

describe("NavbarLinks view test", () => {
  let wrapper;
  let $t;
  let $i18n;

  beforeEach(() => {
    $t = jest.fn();
    $i18n = {
      locale: "pt",
      availableLocales: ["pt", "en"],
    };

    wrapper = shallowMount(NavbarLinks, {
      global: {
        mocks: {
          $t,
          $i18n,
        },
      },
    });
  });

  it("is expected to render", () => {
    expect(wrapper).toBeDefined();
  });

  describe("view elements", () => {
    ["about", "services", "customers", "team", "contact"].forEach((section) => {
      it(`is expected to have a link to ${section}`, () => {
        const link = wrapper.find(`a.nav-link[href='#${section}']`);

        expect(link.exists()).toBe(true);
      });
    });

    it("is expected to render the current locale", () => {
      const localeToggle = wrapper.find("a.dropdown-toggle");

      expect(localeToggle.text()).toEqual($i18n.locale);
    });

    it("is expected to render the other locale options", () => {
      $i18n.availableLocales
        .filter((locale) => {
          return locale !== $i18n.locale;
        })
        .forEach((locale) => {
          expect(wrapper.text()).toContain(locale);
        });
    });
  });

  describe("methods", () => {
    describe("setLocale", () => {
      const locale = "eo";

      beforeEach(() => {
        wrapper.vm.setLocale(locale);
      });

      it("is expected to set i18n locale", () => {
        expect($i18n.locale).toBe(locale);
      });
    });
  });
});
