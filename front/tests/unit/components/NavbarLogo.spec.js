import { shallowMount } from "@vue/test-utils";

import NavbarLogo from "@/components/NavbarLogo.vue";

describe("NavbarLogo view test", () => {
  let wrapper;
  const type = "logo";

  beforeEach(() => {
    wrapper = shallowMount(NavbarLogo, {
      props: {
        type,
      },
    });
  });

  it("is expected to render", () => {
    expect(wrapper).toBeDefined();
  });

  describe("page elements", () => {
    it("is expected to render the logo", () => {
      const webp = wrapper.find("picture source.logo");
      const fallbackPng = wrapper.find("picture img.logo");

      expect(webp.exists()).toBe(true);
      expect(fallbackPng.exists()).toBe(true);
    });
  });
});
