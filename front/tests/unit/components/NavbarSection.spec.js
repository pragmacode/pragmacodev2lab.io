import { shallowMount } from "@vue/test-utils";

import NavbarSection from "@/components/NavbarSection.vue";

describe("NavbarSection view test", () => {
  let wrapper;

  beforeEach(() => {
    wrapper = shallowMount(NavbarSection);
  });

  it("is expected to render", () => {
    expect(wrapper).toBeDefined();
  });

  describe("view elements", () => {
    it("is expected to render the logo", () => {
      const navbarLogo = wrapper.find("navbar-logo-stub");

      expect(navbarLogo.exists()).toBe(true);
    });

    it("is expected to the NavbarLinks", () => {
      const navbarLinks = wrapper.find("navbar-links-stub");

      expect(navbarLinks.exists()).toBe(true);
    });
  });
});
