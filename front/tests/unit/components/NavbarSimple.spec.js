import { shallowMount } from "@vue/test-utils";

import NavbarSimple from "@/components/NavbarSimple.vue";

describe("NavbarSimple view test", () => {
  let wrapper;
  let $router;
  const navClass = "work";
  const logo = "logo";
  const indexSection = "#workers";

  beforeEach(() => {
    $router = {
      push: jest.fn(),
    };

    wrapper = shallowMount(NavbarSimple, {
      props: {
        navClass,
        logo,
        indexSection,
      },
      global: {
        mocks: {
          $router,
        },
      },
    });
  });

  it("is expected to render", () => {
    expect(wrapper).toBeDefined();
  });

  describe("page elements", () => {
    it("is expected to render the logo", () => {
      const navbarLogo = wrapper.find("navbar-logo-stub");

      expect(navbarLogo.exists()).toBe(true);
    });

    it("is expected to render the close button", () => {
      const button = wrapper.find("button.close");
      expect(button.exists()).toBe(true);
    });
  });

  describe("methods", () => {
    describe("goToIndex", () => {
      beforeEach(() => {
        wrapper.vm.goToIndex();
      });

      it("is expected to push the Index route", () => {
        expect($router.push).toHaveBeenCalledWith({
          name: "IndexPage",
          hash: indexSection,
        });
      });
    });
  });
});
