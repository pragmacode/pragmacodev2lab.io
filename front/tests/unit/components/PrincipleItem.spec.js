import { shallowMount } from "@vue/test-utils";

import PrincipleItem from "@/components/PrincipleItem.vue";

describe("PrincipleItem view test", () => {
  let wrapper;
  const imgName = "logo";
  const title = "The title";
  const text = "Very descriptive text";
  let imageSource = jest.fn();

  beforeEach(() => {
    wrapper = shallowMount(PrincipleItem, {
      props: {
        imgName,
        title,
        text,
      },
      methods: {
        imageSource,
      },
    });
  });

  it("is expected to render", () => {
    expect(wrapper).toBeDefined();
  });

  it("is expected to render the image", () => {
    const source = wrapper.find("picture source");
    expect(source.exists()).toBe(true);

    const image = wrapper.find("picture img");
    expect(image.exists()).toBe(true);
  });

  it("is expected to render the title", () => {
    const titleContainer = wrapper.find(".subtitle");
    expect(titleContainer.text()).toContain(title);
  });

  it("is expected to render the description", () => {
    const textContainer = wrapper.find(".text");
    expect(textContainer.text()).toContain(text);
  });

  describe("methods", () => {
    beforeEach(() => {
      wrapper = shallowMount(PrincipleItem, {
        props: {
          imgName,
          title,
          text,
        },
      });
    });

    describe("imageSource", () => {
      const extension = "png";
      let returnedValue;

      beforeEach(() => {
        returnedValue = wrapper.vm.imageSource(extension);
      });

      it("is expected to return the image path with the given extension", () => {
        expect(returnedValue).toEqual(
          require(`@/assets/${imgName}.${extension}`)
        );
      });
    });
  });
});
