import { shallowMount } from "@vue/test-utils";

import PrinciplesSection from "@/components/PrinciplesSection.vue";

describe("PrinciplesSection view test", () => {
  let wrapper;
  let $t;
  const translatedText = "text in another language";

  beforeEach(() => {
    $t = jest.fn().mockReturnValue(translatedText);

    wrapper = shallowMount(PrinciplesSection, {
      global: {
        mocks: {
          $t,
        },
      },
    });
  });

  it("is expected to render", () => {
    expect(wrapper).toBeDefined();
  });

  ["excellence", "commitment", "colab_work"].forEach((column) => {
    it(`is expected to translate the Principle for ${column}`, () => {
      expect($t).toHaveBeenCalledWith(`components.principles.${column}_title`);
      expect($t).toHaveBeenCalledWith(`components.principles.${column}_text`);
    });
  });

  it("is expected to render the Principle component", () => {
    const principles = wrapper.findAllComponents("Principle-Item-stub");
    expect(principles).toHaveLength(3);
  });
});
