/* jscpd:ignore-start */
import { shallowMount } from "@vue/test-utils";

import ServiceSmallImage from "@/components/ServiceSmallImage.vue";

describe("ServiceSmallImage view test", () => {
  let wrapper;

  beforeEach(() => {
    wrapper = shallowMount(ServiceSmallImage);
  });

  it("is expected to render", () => {
    expect(wrapper).toBeDefined();
  });

  it("is expected to render the image", () => {
    const webp = wrapper.find("picture source");
    const fallbackPng = wrapper.find("picture img");

    expect(webp.exists()).toBe(true);
    expect(fallbackPng.exists()).toBe(true);
  });
});
/* jscpd:ignore-end */
