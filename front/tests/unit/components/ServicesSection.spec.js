/* jscpd:ignore-start */
import { shallowMount } from "@vue/test-utils";

import router from "@/router";
import ServicesSection from "@/components/ServicesSection.vue";

describe("ServicesSection view test", () => {
  let wrapper;
  let $t;
  const translatedText = "text in another language";

  beforeEach(() => {
    $t = jest.fn().mockReturnValue(translatedText);
    wrapper = shallowMount(ServicesSection, {
      global: {
        mocks: {
          $t,
        },
        plugins: [router],
      },
    });
  });

  it("is expected to render", () => {
    expect(wrapper).toBeDefined();
  });

  it("is expected to render the title", () => {
    expect($t).toHaveBeenCalledWith("components.services.title");

    const titleParagraph = wrapper.find("p.title");
    expect(titleParagraph.text()).toContain(translatedText);
  });

  it("is expected to render the description first paragraph", () => {
    expect($t).toHaveBeenCalledWith("components.services.description");

    const descriptionParagraph = wrapper.find("p.services-description");
    expect(descriptionParagraph.text()).toContain(translatedText);
  });

  it("is expected to render the separator image", () => {
    const webp = wrapper.find("picture source");
    const fallbackPng = wrapper.find("picture img");

    expect(webp.exists()).toBe(true);
    expect(fallbackPng.exists()).toBe(true);
  });
});
/* jscpd:ignore-end */
