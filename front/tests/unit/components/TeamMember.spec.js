import { shallowMount } from "@vue/test-utils";

import TeamMember from "@/components/TeamMember.vue";

describe("TeamMember view test", () => {
  let wrapper;
  let $t;
  const translatedText = "text in another language";
  const name = "diego";

  beforeEach(() => {
    $t = jest.fn().mockReturnValue(translatedText);

    wrapper = shallowMount(TeamMember, {
      global: {
        mocks: {
          $t,
        },
      },
      props: {
        name,
      },
    });
  });

  it("is expected to render", () => {
    expect(wrapper).toBeDefined();
  });

  it("is expected to render the photo", () => {
    const webp = wrapper.find("picture source");
    const fallbackPng = wrapper.find("picture img");

    expect(webp.exists()).toBe(true);
    expect(fallbackPng.exists()).toBe(true);
  });

  it("is expected to render the frame", () => {
    const frame = wrapper.find("div.team-member-frame");
    const frameFill = wrapper.find("div.team-member-frame-fill");

    expect(frame.exists()).toBe(true);
    expect(frameFill.exists()).toBe(true);
  });

  it("is expected to render the name", () => {
    expect($t).toHaveBeenCalledWith(`components.teamMember.${name}.name`);

    const nameDiv = wrapper.find("div.team-member-name");
    expect(nameDiv.text()).toContain(translatedText);
  });

  it("is expected to render the bio", () => {
    expect($t).toHaveBeenCalledWith(`components.teamMember.${name}.bio`);

    const nameDiv = wrapper.find("div.team-member-bio");
    expect(nameDiv.text()).toContain(translatedText);
  });
});
