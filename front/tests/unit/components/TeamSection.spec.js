import { shallowMount } from "@vue/test-utils";

import TeamSection from "@/components/TeamSection.vue";

describe("TeamSection view test", () => {
  let wrapper;
  let $t;
  const translatedText = "text in another language";

  beforeEach(() => {
    $t = jest.fn().mockReturnValue(translatedText);

    wrapper = shallowMount(TeamSection, {
      global: {
        mocks: {
          $t,
        },
      },
    });
  });

  it("is expected to render", () => {
    expect(wrapper).toBeDefined();
  });

  it("is expected to render the title", () => {
    expect($t).toHaveBeenCalledWith("components.team.title");

    const titleParagraph = wrapper.find("p.team-title");
    expect(titleParagraph.text()).toContain(translatedText);
  });

  it("is expected to render the team members", () => {
    const teamMember = wrapper.find("team-member-stub");
    expect(teamMember.exists()).toBe(true);
  });
});
