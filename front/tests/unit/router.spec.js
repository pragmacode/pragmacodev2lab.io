import router from "@/router";

describe("router", () => {
  it("is expected to route / to IndexPage", () => {
    const route = router.resolve("/");

    expect(route.name).toEqual("IndexPage");
  });

  it("is expected to route /services/prototyping to PrototypingServiceDescription", () => {
    const route = router.resolve("/services/prototyping");

    expect(route.name).toEqual("PrototypingServiceDescription");
  });

  it("is expected to route /services/development to DevelopmentServiceDescription", () => {
    const route = router.resolve("/services/development");

    expect(route.name).toEqual("DevelopmentServiceDescription");
  });

  it("is expected to route /services/maintenance to MaintenanceServiceDescription", () => {
    const route = router.resolve("/services/maintenance");

    expect(route.name).toEqual("MaintenanceServiceDescription");
  });

  it("is expected to route /cases/pae to PaeCaseDescription", () => {
    const route = router.resolve("/cases/pae");

    expect(route.name).toEqual("PaeCaseDescription");
  });

  it("is expected to route /cases/interscity to InterscityCaseDescription", () => {
    const route = router.resolve("/cases/interscity");

    expect(route.name).toEqual("InterscityCaseDescription");
  });

  it("is expected to route /cases/pae to VetorCaseDescription", () => {
    const route = router.resolve("/cases/vetor");

    expect(route.name).toEqual("VetorCaseDescription");
  });

  it("is expected to route /contact/success to SuccessfulContact", () => {
    const route = router.resolve("/contact/success");

    expect(route.name).toEqual("SuccessfulContact");
  });

  it("is expected to route /contact/failed to FailedContact", () => {
    const route = router.resolve("/contact/failed");

    expect(route.name).toEqual("FailedContact");
  });

  it("is expected to route /technical-advice to TechnicalAdvice", () => {
    const route = router.resolve("/technical-advice");

    expect(route.name).toEqual("TechnicalAdvice");
  });

  it("is expected to route /anyotherpath to PageNotFound", () => {
    const route = router.resolve("/anyotherpath");

    expect(route.name).toEqual("PageNotFound");
  });
});
