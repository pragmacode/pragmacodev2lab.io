import { shallowMount } from "@vue/test-utils";

import CaseDescription from "@/views/CaseDescription.vue";

describe("CaseDescription view test", () => {
  let wrapper;
  let $t;
  const translatedText = "text in another language";
  const caseDescription = "work";

  beforeEach(() => {
    $t = jest.fn().mockReturnValue(translatedText);

    wrapper = shallowMount(CaseDescription, {
      props: { caseDescription },
      global: {
        mocks: {
          $t,
        },
      },
    });
  });

  it("is expected to render", () => {
    expect(wrapper).toBeDefined();
  });

  describe("page elements", () => {
    it("is expected to render the simple navbar", () => {
      const button = wrapper.find("navbar-simple-stub");
      expect(button.exists()).toBe(true);
    });

    it("is expected to render the square image", () => {
      const webp = wrapper.find("picture source.squares-case");
      const fallbackPng = wrapper.find("picture img.squares-case");

      expect(webp.exists()).toBe(true);
      expect(fallbackPng.exists()).toBe(true);
    });

    it("is expected to render the title", () => {
      expect($t).toHaveBeenCalledWith(`components.cases.${caseDescription}`);

      const titleParagraph = wrapper.find("div.case-description-title");
      expect(titleParagraph.text()).toContain(translatedText);
    });

    it("is expected to render the description sections", () => {
      const descriptionSection = wrapper.find("case-description-section-stub");
      expect(descriptionSection.exists()).toBe(true);
    });
  });
});
