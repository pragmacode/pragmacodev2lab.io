import { shallowMount } from "@vue/test-utils";

import router from "@/router";

import ContactResponse from "@/views/ContactResponse.vue";

describe("ContactResponse view test", () => {
  let wrapper;
  let $t;
  const translatedText = "text in another language";
  const status = "failed";

  beforeEach(() => {
    $t = jest.fn().mockReturnValue(translatedText);
    wrapper = shallowMount(ContactResponse, {
      global: {
        mocks: {
          $t,
        },
        plugins: [router],
      },
      props: { status },
    });
  });

  it("is expected to render", () => {
    expect(wrapper).toBeDefined();
  });

  it("is expected to render the logo", () => {
    const webp = wrapper.find("picture source.logo-contact-response");
    const fallbackPng = wrapper.find("picture img.logo-contact-response");

    expect(webp.exists()).toBe(true);
    expect(fallbackPng.exists()).toBe(true);
  });

  it(`is expected to render the ${status} message`, () => {
    expect($t).toHaveBeenCalledWith(`views.contact-response.${status}`, {
      email: "contato@pragmacode.com.br",
    });

    const errorParagraph = wrapper.find("p");
    expect(errorParagraph.text()).toContain(translatedText);
  });

  it("is expected to render the link to index", () => {
    const link = wrapper.find("router-link-stub");

    expect(link.exists()).toBe(true);
  });
});
