import { shallowMount } from "@vue/test-utils";

import IndexPage from "@/views/IndexPage.vue";

describe("IndexPage view test", () => {
  let wrapper;
  let $t;
  const translatedText = "text in another language";

  beforeEach(() => {
    $t = jest.fn().mockReturnValue(translatedText);
    wrapper = shallowMount(IndexPage, {
      global: {
        mocks: {
          $t,
        },
      },
    });
  });

  it("is expected to render", () => {
    expect(wrapper).toBeDefined();
  });
});
