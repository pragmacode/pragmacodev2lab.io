import { shallowMount } from "@vue/test-utils";

import router from "@/router";

import PageNotFound from "@/views/PageNotFound.vue";

describe("PageNotFound view test", () => {
  let wrapper;
  let $t;
  const translatedText = "text in another language";

  beforeEach(() => {
    $t = jest.fn().mockReturnValue(translatedText);
    wrapper = shallowMount(PageNotFound, {
      global: {
        mocks: {
          $t,
        },
        plugins: [router],
      },
    });
  });

  it("is expected to render", () => {
    expect(wrapper).toBeDefined();
  });

  it("is expected to render the logo", () => {
    const webp = wrapper.find("picture source.logo-404");
    const fallbackPng = wrapper.find("picture img.logo-404");

    expect(webp.exists()).toBe(true);
    expect(fallbackPng.exists()).toBe(true);
  });

  it("is expected to render the error message", () => {
    expect($t).toHaveBeenCalledWith("views.page-not-found.error");

    const errorParagraph = wrapper.find("p");
    expect(errorParagraph.text()).toContain(translatedText);
  });

  it("is expected to render the link to index", () => {
    const link = wrapper.find("router-link-stub");

    expect(link.exists()).toBe(true);
  });
});
