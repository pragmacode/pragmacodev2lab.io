import { shallowMount } from "@vue/test-utils";

import ServiceDescription from "@/views/ServiceDescription.vue";

describe("ServiceDescription view test", () => {
  let wrapper;
  let $t;
  const translatedText = "text in another language";
  const service = "work";

  beforeEach(() => {
    $t = jest.fn().mockReturnValue(translatedText);

    wrapper = shallowMount(ServiceDescription, {
      props: { service },
      global: {
        mocks: {
          $t,
        },
      },
    });
  });

  it("is expected to render", () => {
    expect(wrapper).toBeDefined();
  });

  describe("page elements", () => {
    it("is expected to render the simple navbar", () => {
      const button = wrapper.find("navbar-simple-stub");
      expect(button.exists()).toBe(true);
    });

    it("is expected to render the title", () => {
      expect($t).toHaveBeenCalledWith(`components.services.${service}`);

      const titleParagraph = wrapper.find("h1.service-description-title");
      expect(titleParagraph.text()).toContain(translatedText);
    });

    it("is expected to render the description", () => {
      expect($t).toHaveBeenCalledWith(`views.service-description.${service}`);

      const titleParagraph = wrapper.find("p.service-description");
      expect(titleParagraph.text()).toContain(translatedText);
    });

    it("is expected to render the graphic", () => {
      const webp = wrapper.find(
        "div.service-description-container picture source"
      );
      const fallbackPng = wrapper.find(
        "div.service-description-container picture img"
      );

      expect(webp.exists()).toBe(true);
      expect(fallbackPng.exists()).toBe(true);
    });
  });
});
