import { shallowMount } from "@vue/test-utils";

import TechnicalAdvice from "@/views/TechnicalAdvice.vue";

describe("TechnicalAdvice view test", () => {
  let wrapper;
  let $t;
  const translatedText = "text in another language";

  beforeEach(() => {
    $t = jest.fn().mockReturnValue(translatedText);

    wrapper = shallowMount(TechnicalAdvice, {
      global: {
        mocks: { $t },
      },
    });
  });

  it("is expected to render", () => {
    expect(wrapper).toBeDefined();
  });

  it("is expected to render the title", () => {
    expect($t).toHaveBeenCalledWith("views.technical-advice.title");

    const errorParagraph = wrapper.find("p.title");
    expect(errorParagraph.text()).toContain(translatedText);
  });

  it("is expected to render the description", () => {
    expect($t).toHaveBeenCalledWith("views.technical-advice.description");

    const description = wrapper.find("p.description");
    expect(description.text()).toContain(translatedText);
  });

  it("is expected to render the contact description", () => {
    expect($t).toHaveBeenCalledWith(
      "views.technical-advice.contactDescription"
    );

    const contactDescription = wrapper.find("div#contact p");
    expect(contactDescription.text()).toContain(translatedText);
  });

  it("is expected to renders the ContactForm component", () => {
    const contactForm = wrapper.find("contact-form-stub");
    expect(contactForm.exists()).toBe(true);
  });

  it("is expected to render the intro art", () => {
    const webp = wrapper.find("picture source.ta_lp_intro");
    const fallbackPng = wrapper.find("picture img.ta_lp_intro");

    expect(webp.exists()).toBe(true);
    expect(fallbackPng.exists()).toBe(true);
  });

  it("is expected to render the LandingPageSection component", () => {
    const landingPageSection = wrapper.find("landing-page-section-stub");
    expect(landingPageSection.exists()).toBe(true);
  });

  it("is expected to render the botton contact text", () => {
    expect($t).toHaveBeenCalledWith("views.technical-advice.footerContactText");

    const bottonContactText = wrapper.find(".footer-contact p.subtitle");
    expect(bottonContactText.text()).toContain(translatedText);
  });

  it("is expected to render the botton contact link", () => {
    expect($t).toHaveBeenCalledWith(
      "views.technical-advice.footerContactButton"
    );

    const bottonContactButton = wrapper.find(".footer-contact a");
    expect(bottonContactButton.text()).toContain(translatedText);
  });
});
