const path = require("path");
const PrerenderSPAPlugin = require("prerender-spa-plugin");
const SitemapPlugin = require("sitemap-webpack-plugin").default;

const paths = [
  "/",
  "/services/prototyping",
  "/services/development",
  "/services/maintenance",
  "/cases/pae",
  "/cases/interscity",
  "/cases/vetor",
  "/contact/success",
  "/contact/failed",
];

module.exports = {
  publicPath: "/",
  chainWebpack: (config) => {
    config.plugins.delete("prefetch");
    config.plugins.delete("preload");
  },
  configureWebpack: () => {
    if (process.env.NODE_ENV !== "production")
      return {
        resolve: {
          alias: {
            jquery: "jquery/dist/jquery.slim.min.js",
            "vue-i18n": "vue-i18n/dist/vue-i18n.esm-browser.prod.js",
          },
        },
      };
    return {
      plugins: [
        new PrerenderSPAPlugin({
          staticDir: path.join(__dirname, "dist"),
          routes: paths,
        }),
        new SitemapPlugin({
          base: "https://www.pragmacode.com.br",
          paths: paths,
        }),
      ],
      resolve: {
        alias: {
          jquery: "jquery/dist/jquery.slim.min.js",
          "vue-i18n": "vue-i18n/dist/vue-i18n.esm-browser.prod.js",
        },
      },
    };
  },
};
